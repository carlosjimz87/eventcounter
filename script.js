const EVENT_DATE_STR = "21 Dec 2020";

// linking DOM elements
const body = document.getElementsByTagName("BODY")[0];
const searchBtn = document.getElementById("search-btn");
const searchTxt = document.getElementById("search-txt");
const searchBox = document.getElementById("search-box");
const daysEl = document.getElementById("days");
const hoursEl = document.getElementById("hours");
const minsEl = document.getElementById("mins");
const secondsEl = document.getElementById("seconds");

var secondsToEvent = (eventDateStr) => {
  const event = new Date(eventDateStr);
  const today = new Date();
  return (event - today) / 1000;
};

var countDown = () => {
  const totalSeconds = secondsToEvent(EVENT_DATE_STR);

  const days = !isNaN(totalSeconds) ? Math.floor(totalSeconds / 3600 / 24) : 0;

  const hours = !isNaN(totalSeconds)
    ? Math.floor((totalSeconds / 3600) % 24)
    : 0;
  const mins = !isNaN(totalSeconds) ? Math.floor(totalSeconds / 60) % 60 : 0;
  const seconds = !isNaN(totalSeconds) ? Math.floor(totalSeconds) % 60 : 0;

  daysEl.innerHTML = days;
  hoursEl.innerHTML = formatTime(hours);
  minsEl.innerHTML = formatTime(mins);
  secondsEl.innerHTML = formatTime(seconds);

  if (isNaN(totalSeconds)) {
    searchTxt.placeholder = "Invalid date, retry!";
  }
};

function formatTime(time) {
  return time < 10 ? `0${time}` : time;
}

// DOM events
searchBtn.addEventListener("click", function () {
  body.classList.add("forward");
});

// main
function init() {
  if (navigator.userAgent.match(/Mobile/)) {
    searchTxt.placeholder = "";
  } else {
    searchTxt.placeholder = "Search for events...";
  }

  setInterval(countDown, 1000);
}

window.onload = init;
