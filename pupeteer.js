const puppeteer = require("puppeteer");
const fs = require("fs");

var searches = 1;
function repeatSearch(query, val, err) {
  if (err) {
    if (searches < 2) {
      searches++;
      gsearch(query, true);
      return true;
    } else {
      console.log("ERROR: No date results");
      return false;
    }
  }
  if (val && val.length > 0) {
    return false;
  } else {
    searches++;
    gsearch(query, true);
    return true;
  }
}

function formatQuery(input, before) {
  if (!before) return !input.includes("día") ? input + " día " : input;
  else return !input.includes("día") ? "día " + input : input;
}

async function gsearch(query = "", before = false) {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      "--no-sandbox",
      "--fast-start",
      "--disable-setuid-sandbox",
      "--disable-extensions",
    ],
    ignoreHTTPSErrors: true,
  });

  const page = await browser.newPage();

  var cookies = [
    {
      name: "CONSENT",
      value: "YES+ES.es+V14+BX",
      domain: ".google.es",
    },
  ];

  await page.setCookie(...cookies);
  await page.goto("https://google.es");
  if (query.length == 0) {
    let array = await fs.readFileSync("words.txt").toString().split("\n");
    query = array[Math.floor(Math.random() * array.length)];
  }
  // simple selector for search box
  await page.click("[name=q]");
  let typedquery = formatQuery(query, before);
  console.log("SEARCH: ", typedquery);
  await page.keyboard.type(typedquery);

  await page.keyboard.press("Enter");
  // wait for search results
  await page.waitForSelector(".zCubwf", { timeout: 5000 }).catch((err) => {
    repeatSearch(query, null, err);
  });

  const val = await page.evaluate(
    () => document.querySelector(".zCubwf").textContent
  );

  if (!repeatSearch(query, val, null)) {
    console.log(val);
  }
}

gsearch();
