## TODO

[✓] - Counter timer.
[✓] - Change background on button press.
[✓] - Find event dates with pupeteer and Google Serp Answer Box (GSAB).
[✓] - Input search + styles + transitions.

[ ] - Improve input search transition.
https://www.youtube.com/watch?v=Ep2aDb5ONiQ
[ ] - Create a search input to find events.
https://www.youtube.com/watch?v=MBJuTkILZYo

[ ] - Automcomplete search
[ ] - Attach search events results to corresponding dates in the search input (from GSAB).
[ ] - Create a dialog for custom events.
[ ] - Change brackground for each events using some color categorization tool.
[ ] - Validations and testing.
[ ] - Making the web fully responsive.

[ ] - Changing static web to NodeJS web service.
[ ] - Deploy on cloud.
[ ] - Define dev, prod and testing environments if possible.
